#!/usr/bin/make -f
# -*- makefile -*-

DEB_VERSION := $(shell dpkg-parsechangelog --format rfc822 | grep-dctrl -ensVersion -S .)

DH_VERBOSE=1

export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

%:
	dh $@

override_dh_autoreconf:
	dh_autoreconf --as-needed

override_dh_auto_configure:
	dh_auto_configure -- --localstatedir=/var/lib --enable-embedded-perl --libdir=/usr/lib

override_dh_shlibdeps:
	dh_shlibdeps
	dpkg-shlibdeps -Tdebian/mod-gearman-module.substvars debian/mod-gearman-module/usr/lib/mod_gearman/mod_gearman.o

override_dh_auto_install:
	dh_auto_install
	rm -f debian/mod-gearman/usr/share/mod_gearman/shared.conf
	rm -f debian/mod-gearman/usr/share/mod_gearman/standalone_worker.conf

	@ # Fix permissions
	chmod 644 $(CURDIR)/debian/tmp/usr/share/mod_gearman/mod_gearman_p1.pl

	help2man --no-info --section=8 --version-string="mod_gearman_worker $(DEB_VERSION)" \
		--help-option=-h --include=$(CURDIR)/debian/help2man.include \
                -n "Agent that runs active checks from a gearman queue" \
                $(CURDIR)/debian/tmp/usr/bin/mod_gearman_worker \
                > $(CURDIR)/debian/mod_gearman_worker.8

	help2man --no-info --section=8 --version-string="check_gearman $(DEB_VERSION)" \
		--help-option=-h --include=$(CURDIR)/debian/help2man.include \
                -n "Nagios service check to monitor the gearman job server" \
                $(CURDIR)/debian/tmp/usr/bin/check_gearman \
                > $(CURDIR)/debian/check_gearman.8

	help2man --no-info --section=8 --version-string="send_gearman $(DEB_VERSION)" \
		--help-option=-h --include=$(CURDIR)/debian/help2man.include \
                -n "Submit active and passive check results to a gearman job server" \
                $(CURDIR)/debian/tmp/usr/bin/send_gearman \
                > $(CURDIR)/debian/send_gearman.8

	help2man --no-info --section=8 --version-string="gearman_top $(DEB_VERSION)" \
		--help-option=-h --include=$(CURDIR)/debian/help2man.include \
                -n "Monitor the gearman job server" \
                $(CURDIR)/debian/tmp/usr/bin/gearman_top \
                > $(CURDIR)/debian/gearman_top.8

	help2man --no-info --section=8 --version-string="send_multi $(DEB_VERSION)" \
		--help-option=-h --include=$(CURDIR)/debian/help2man.include \
                -n "Submit check results from check_multi to a gearman job server" \
                $(CURDIR)/debian/tmp/usr/bin/send_multi \
                > $(CURDIR)/debian/send_multi.8

	help2man --no-info --section=8 --version-string="mod_gearman_mini_epn $(DEB_VERSION)" \
		--help-option=-h --include=$(CURDIR)/debian/help2man.include \
                -n "Verify that a plugin can be run from the embedded perl interpreter (EPN)" \
                $(CURDIR)/debian/tmp/usr/bin/mod_gearman_mini_epn \
                > $(CURDIR)/debian/mod_gearman_mini_epn.8

override_dh_auto_test:
