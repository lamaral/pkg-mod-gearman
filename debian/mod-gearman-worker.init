#!/bin/sh

### BEGIN INIT INFO
# Provides:          mod-gearman-worker
# Required-Start:    $local_fs $remote_fs
# Required-Stop:     $local_fs $remote_fs
# Should-Start:
# Should-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Control the mod-gearman worker daemon
# Description:       Control the mod-gearman worker daemon which fetches
#                    and processes jobs (servicechecks, hostchecks or
#                    eventhandlers) from a gearman-job-server instance.
### END INIT INFO


DESC="Mod-Gearman worker agents"
PATH=/usr/sbin:/usr/bin:/sbin:/bin
NAME=mod_gearman_worker
DAEMON=/usr/sbin/$NAME
CONFIG=/etc/mod-gearman/worker.conf
PIDFILE=/run/mod-gearman/worker.pid
USER=nagios
USERID=$(id -u)
DAEMON_OPTS="-d --config=$CONFIG --pidfile=$PIDFILE --logmode=syslog"

# Bail out if not installed
[ -x "$DAEMON" ] || exit 0

. /lib/lsb/init-functions

if [ -e /etc/default/mod-gearman-worker ]; then
    . /etc/default/mod-gearman-worker
fi

# this is from madduck on IRC, 2006-07-06
# There should be a better possibility to give daemon error messages
# and/or to log things
log() {
    case "$1" in
        [[:digit:]]*) success=$1; shift;;
        *) :;;
    esac
    log_action_begin_msg "$1"; shift
    log_action_end_msg ${success:-0} "$*"
}

abort_if_unconfigured() {
    if [ ! -e "$CONFIG" ]; then
        log 1 "Configuration file $CONFIG not present"
        exit 1
    fi
}

pre_start() {
    install -o $USER -g $USER -d $(dirname $PIDFILE)
}

start_worker() {
    start-stop-daemon --start --oknodo --user $USER --exec $DAEMON --quiet \
        --chuid $USER --pidfile $PIDFILE -- $DAEMON_OPTS || return 2
}

stop_worker() {
    start-stop-daemon --stop --oknodo --user $USER --exec $DAEMON --quiet \
        --retry 10 --pidfile $PIDFILE
    RETVAL="$?"
    [ "$RETVAL" = 2 ] && return 2
    # Wait for children to finish too if this is a daemon that forks
    # and if the daemon is only ever run from this initscript.
    # If the above conditions are not satisfied then add some other code
    # that waits for the process to drop all resources that could be
    # needed by services started subsequently.  A last resort is to
    # sleep for some time.
    start-stop-daemon --stop --quiet --oknodo --retry=0/30/KILL/5 --user $USER --exec $DAEMON
    [ "$?" = 2 ] && return 2
}

reload_worker() {
    start-stop-daemon --stop --oknodo --user $USER --exec $DAEMON --quiet \
        --signal HUP --pidfile $PIDFILE
    return 0
}

status_worker() {
    status_of_proc -p $PIDFILE $DAEMON $NAME && exit 0 || exit $?
}

case "$1" in
    start)
        abort_if_unconfigured
        log_daemon_msg "Starting $DESC" "$NAME"
        pre_start
        start_worker
        case "$?" in
            0|1) log_end_msg 0 ;;
            2) log_end_msg 1 ;;
        esac
        ;;
    stop)
        log_daemon_msg "Stopping $DESC" "$NAME"
        stop_worker
        case "$?" in
            0|1) log_end_msg 0 ;;
            2) log_end_msg 1 ;;
        esac
        ;;
    reload|force-reload)
        log_daemon_msg "Reloading $DESC" "$NAME"
        reload_worker
        log_end_msg $?
        ;;
    status)
        status_worker && exit 0 || exit $?
        ;;
    restart)
        abort_if_unconfigured
        log_daemon_msg "Restarting $DESC" "$NAME"
        stop_worker
        case "$?" in
            0|1)
                start_worker
                case "$?" in
                    0) log_end_msg 0 ;;
                    1) log_end_msg 1 ;; # Old process is still running
                    *) log_end_msg 1 ;; # Failed to start
                esac
                ;;
            *)
                # Failed to stop
                log_end_msg 1
                ;;
        esac
        ;;
    *)
        echo "Usage: $0 [start|stop|status|reload|force-reload|restart]"
        exit 1
        ;;
esac

exit 0
